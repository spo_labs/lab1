//
// Created by itmo on 9/21/23.
//

#include <stdio.h>
#include "node.h"
#include <string.h>
#include <stdlib.h>

Node **nodes;
u_int64_t countNodes;

Node *createNode(char *nameNode, Node *left, Node *right, char *value) {
    Node *node = malloc(sizeof(Node));
    node->node_id = countNodes;
    node->nameNode = nameNode;
    node->left = left;
    node->right = right;
    char *val_buf = malloc(1024 * sizeof(char));
    strcpy(val_buf, value);
    node->value = val_buf;
    nodes[countNodes] = node;
    countNodes += 1;
    return node;
}

void printNodeWithId(Node *node) {
    printf("[Id: %d; Type: %s", node->node_id, node->nameNode);
    strlen(node->value) > 0 ? printf("; Val: %s]", node->value) : printf("]");
}

void printNodeLeft(Node *node) {
    printNodeWithId(node);
    printf(" -> ");
    printNodeWithId(node->left);
    printf("\n");
}

void printNodeRight(Node *node) {
    printNodeWithId(node);
    printf(" -> ");
    printNodeWithId(node->right);
    printf("\n");
}

void printNode(Node *node) {
    if (node->left) {
        printNodeLeft(node);
        printNode(node->left);
    }
    if (node->right) {
        printNodeRight(node);
        printNode(node->right);
    }
}


void printNodes() {
    printNode(nodes[countNodes - 1]);
    printNodeWithId(nodes[countNodes - 1]);
}



