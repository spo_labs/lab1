//
// Created by itmo on 9/21/23.
//

#ifndef LAB_1_AST_H
#define LAB_1_AST_H

typedef struct Node Node;

struct Node {
    int node_id;
    char *nameNode;
    Node *left;
    Node *right;
    char *value;
};

void printNodes();

Node *createNode(char *nameNode, Node *left, Node *right, char *value);


#endif //LAB_1_AST_H
