//
// Created by itmo on 9/22/23.
//

#include "main_lab_1.h"
#include "node.h"

extern Node ** nodes;
extern u_int64_t countNodes;


int main(int argc, char *argv[]) {
    nodes = malloc(1024 * 8 * sizeof(Node*));
    countNodes = 0;
    if (argc <= 4) {
        FILE *input_file = fopen(argv[1], "r");
        if (input_file) {
            yyin = input_file;
            yyparse();
            fclose(input_file);
            printNodes();
        } else {
            printf("Не удалось открыть файл: %s\n", argv[1]);
        }
    } else {
        printf("Использование: %s <input_file>\n", argv[0]);
    }
    return 0;
}